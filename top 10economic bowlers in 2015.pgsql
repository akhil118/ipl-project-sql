
-----------------------------4-----------------------------

-- quary for Top 10 economical bowlers in the year 2015


SELECT bowler,(CAST((total_runs/total_overs) AS FLOAT)) AS economy FROM 
(
SELECT bowler , sum(total_runs) as total_runs,
(CAST((count(total_runs)/6) AS FLOAT)) AS total_overs
FROM matches INNER JOIN deliveries
on matches.id = deliveries.matchid
WHERE season = 2015
GROUP BY bowler
) 
AS data
ORDER BY economy
limit 10
