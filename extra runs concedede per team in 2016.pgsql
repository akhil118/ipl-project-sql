------------------------3---------------------
-- quary for Extra runs conceded per team in the year 2016

SELECT season,batting_team
,sum(extra_runs) AS extra_runs_conceded FROM matches INNER JOIN deliveries
on matches.id = deliveries.matchid
WHERE season = 2016
GROUP BY batting_team,season
ORDER BY extra_runs_conceded DESC
